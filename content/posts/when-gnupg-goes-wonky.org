#+title: Early GnuPG2 was weird and had most unhelpful error messages
#+date: 2018-03-15 23:42:00
#+categories[]: configuration
#+tags[]: GnuPG gpg2 Emacs configuration UI Debian

Last edited: [2022-06-17 Fri] - Rewrote the article with new tone and
corrected content.

I had recently set up GnuPG at work after a long while of not touching
it, and saw that it worked without issue, on first try.

So I decided to try at home as I hadn't set it back up since I had
last reinstalled my OS when switching distributions. Unfortunately, I
didn't realize at first that some critical parts were missing, and I'd
soon find out when trying to decrypt some email that GnuPG has some
pretty unhelpful error messages.

#+BEGIN_QUOTE
Inappropriate ioctl for device
...
[GNUPG:] ERROR pkdecrypt_failed 83918950
#+END_QUOTE

That was all that it would tell me, an error which effectively amounts
to "something failed to decrypt" on enabling debug in Emacs.

I spent a while looking it up, but didn't ever really find the answer
to my issues, because that error occurs above the layer that actually
failed in my setup.

It turns out that the only =pinentry= implementation I had on my
machine was the ncurses-based one, which is neither usable from Emacs
nor any graphical MUAs (setting up mutt for my *numerous* accounts was
beyond my motivation at the time; although with time I've lost any
interest in non-Emacs MUAs). I already knew from testing it that GPG
*did* work if I tried to encrypt or decrypt files from the terminal,
so it was clear that it wasn't simply broken.

The original version of this post went for some while about the
~pinentry.el~ package, but as it turns out, I hadn't actually set that
up properly and it wasn't related to my fixing it back then.

The fix turned out to be as simple as installing =pinentry-qt= on my
system.

That's it. Do note that there is exactly *no* indication in the above
error that the problem has anything to do with =pinentry=. Why GnuPG?
Why?

In any case, lesson learned. If building a system from a barebones
Debian Standard install to avoid desktop-environment default
dependencies, make sure to specifically install those few dependencies
they have that are actually useful.


As an aside, ever since GnuPG 2.1.5 ~pinentry.el~ is useless.
