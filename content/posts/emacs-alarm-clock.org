#+title: Using Emacs as an Alarm Clock
#+date: 2023-06-23 03:12:00
#+categories[]: Miscellaneous
#+tags[]: Emacs Miscellaneous Timers mpv

In a bit of silliness and laziness, I now find myself using Emacs as
an alarm clock replacement.

My last one had served me fairly well for the last... over a decade
but finally some of the electronics gave out.

Assuming you've got something like ~mpv~ installed, you can just add
something like the following to your Emacs init config, adjusting of
course for your prefered volume & audio file.

I happen to enjoy using music to wake up.

#+begin_src elisp
  (run-at-time "9:00" (* 24 60 60)
               (lambda ()
                 (let ((song "/home/user/Music/Samurai - Never Fade Away.mp3"))
                   (start-process "alarm-clock" nil
                                  "mpv" "--volume=42" song))))
#+end_src

One can get a lot fancier with said timers and functions for selecting
arbitrary songs, but I'll leave that as an exercise to the reader.
